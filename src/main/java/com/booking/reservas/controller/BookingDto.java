package com.booking.reservas.controller;

import java.util.Date;

public class BookingDto {

    private long oid;
    private final String id;
    private final String name;
    private String email;
    private long phone;
    private Date date;

    public BookingDto(String id, String name) {
        this.id = id;
        this.name = name;

    }

    public long getOid() {return oid;}

    public String getId() {
        return id;
    }

    public String getName() {
        return name;
    }

    public String getEmail() {
        return email;
    }

    public long getPhone() {
        return phone;
    }

    public Date getDate() {
        return date;
    }



}
